import com.github.kittinunf.fuel.Fuel
import com.google.gson.Gson
import org.jsoup.Jsoup
import java.awt.Image
import java.awt.image.BufferedImage
import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import java.net.URI
import java.net.URL
import java.nio.charset.Charset
import java.util.*
import java.util.zip.GZIPOutputStream
import javax.imageio.ImageIO
import javax.xml.stream.XMLOutputFactory


fun readRoutesPoi(root: File, subdir: String? = null, poiList: Map<String, UUID>): List<UUID> {
    val list = mutableListOf<UUID>()
    val routesPoiFile = if (subdir.isNullOrEmpty()) {
        File(root, Const.FILE_ROUTES_POI_LIST)
    } else {
        File(File(root, subdir), Const.FILE_ROUTES_POI_LIST)
    }

    if (routesPoiFile.isFile && routesPoiFile.canRead()) {
        routesPoiFile.readLines().forEach { line ->
            if (poiList.keys.contains(line)) {
                println("  odczytuję route poi ${poiList[line]} -> $line")
                list.add(poiList.getValue(line))
            }
        }
    }
    return list
}

fun readCategories(dir: File) {
    println("Odczytuję kategorie...")

    val metadataFile = File(dir, Const.FILE_METADATA_JSON)
    if (!metadataFile.isFile || !metadataFile.canRead()) {
        return
    }

    // OUT DIR
    val outDir = File(File(Conf.ROOT, Const.DIR_OUT), Const.DIR_OUT_UPDATES)
    if (!outDir.exists()) {
        outDir.mkdirs()
    }

    val categories: Map<String, Category> = try {
        Gson().fromJson(metadataFile.readText(), typeCategoriesHashMap)
    } catch (e: Exception) {
        print(" E: ${metadataFile.canonicalPath} ${e.message}")
        emptyMap()
    }

    // create XML
    val file = File(outDir, Const.GZ_XML_CATEGORIES)
    val outputStream = FileOutputStream(file)
    val outputStreamZip = GZIPOutputStream(outputStream)
    val factory = XMLOutputFactory.newInstance()
    val writer = factory.createXMLStreamWriter(outputStreamZip, "UTF-8")

    writer.document {
        element(Const.XML_TAG_CATEGORIES) {
            categories.forEach { id, c ->
                println("  $id = $c")
                element(Const.XML_TAG_CATEGORY) {
                    attribute(Const.XML_ATTR_ID, id)
                    attribute(Const.XML_ATTR_TAGS, c.tags.joinToString(","))
                    element(Const.XML_TAG_THUMBNAIL) {
                        val thumbnailFile = File(dir, c.thumbnail)
                        attribute(Const.XML_ATTR_ICON, thumbnailFile.nameWithoutExtension)
                        writeCharacters(decodeThumbnail(thumbnailFile))
                    }
                    c.name.forEach { lang, name ->
                        element(Const.XML_TAG_NAME) {
                            if (lang !in arrayOf(Conf.DEFAULT_LANG, String())) {
                                attribute(Const.XML_ATTR_LANG, lang)
                            }
                            writeCharacters(name)
                        }
                    }
                }
            }
        }
    }
    writer.flush()
    writer.close()
    outputStreamZip.close()
    outputStream.close()
    val outFileSize = file.length()

    Conf.updates.add(Update(
            size = outFileSize,
            type = UPDATES.CATEGORIES,
            url = URL(
                    Conf.UPDATES_REMOTE_PROTOCOL,
                    Conf.UPDATES_REMOTE_HOST,
                    File("/" + Const.DIR_OUT_UPDATES, Const.GZ_XML_CATEGORIES).canonicalPath)
    ))
    println("Zapisano kategorie: ${file.canonicalPath}\n")
}

fun readThumbnail(root: File, subdir: String? = null): String {
    val dir = if (subdir.isNullOrEmpty()) {
        root
    } else {
        File(root, subdir)
    }

    return arrayOf(".jpg", ".png")
            .map { File(dir, Const.FILE_THUMBNAIL + it) }
            .firstOrNull { it.exists() }
            ?.let { decodeThumbnail(it) }
            ?: String()
}


fun decodeThumbnail(file: File?): String {
    if (file is File && file.exists() && file.isFile && file.canRead()) {
        return try {
            Base64.getEncoder().encodeToString(file.readBytes())
        } catch (e: Exception) {
            e.message
            String()
        }
    }
    return String()
}


fun readMaps(dir: File) {
    println("Odczytuję mapy...")
    val metadataFile = File(dir, Const.FILE_METADATA_JSON)
    if (!metadataFile.isFile || !metadataFile.canRead()) {
        return
    }

    // OUT MAP DIR
    val outMapDir = File(File(Conf.ROOT, Const.DIR_OUT), Const.DIR_MAPS)
    if (!outMapDir.exists()) {
        outMapDir.mkdirs()
    }

    val maps: Map<String, MapMetadata> = try {
        Gson().fromJson(metadataFile.readText(), typeMapsHashMap)
    } catch (e: Exception) {
        e.message
        emptyMap()
    }

    maps.forEach { name, m ->
        println("  $name = $m")

        val url = if (!m.uri.isAbsolute) {
            val f = File(dir, m.uri.path)
            val target = File(outMapDir, m.uri.path)
            try {
                f.copyTo(target, overwrite = false)
                println("   > ${f.absolutePath}  => $target")
            } catch (e: FileAlreadyExistsException) {
                println("   W: ${f.absolutePath} istnieje. Pomijam.")
            } catch (e: NoSuchFileException) {
                println("   W: ${f.absolutePath} nie istnieje. Pomijam.")
            } catch (e: Exception) {
                e.printStackTrace()
            }
            Pair(URL(
                    Conf.UPDATES_REMOTE_PROTOCOL,
                    Conf.UPDATES_REMOTE_HOST,
                    File("/" + Const.DIR_MAPS, m.uri.path).canonicalPath),
                    f.length())

        } else {
            val remoteFileSize = try {
                m.uri.toURL().openConnection().contentLength.toLong()
            } catch (e: IOException) {
                e.printStackTrace()
                0L
            }

            Pair(m.uri.toURL(), remoteFileSize)
        }

        Conf.updates.add(Update(
                type = UPDATES.MAP,
                uuid = m.uuid,
                name = name,
                url = url.first,
                size = url.second,
                version = m.version
        ))
    }
}


fun writeUpdatesXml() {
    println("Zapisuję plik updates.")
    // OUT DIR
    val outDir = File(File(Conf.ROOT, Const.DIR_OUT), Const.DIR_OUT_UPDATES)
    if (!outDir.exists()) {
        outDir.mkdirs()
    }

    // create XML
    val file = File(outDir, Const.GZ_XML_UPDATES)
    val outputStream = FileOutputStream(file)
    val outputStreamZip = GZIPOutputStream(outputStream)
    val factory = XMLOutputFactory.newInstance()
    val writer = factory.createXMLStreamWriter(outputStreamZip, "UTF-8")

    writer.document {
        element(Const.XML_TAG_UPDATES) {
            Conf.updates.forEach { u ->
                println(" $u")
                val tag = when (u.type) {
                    UPDATES.GUIDEBOOK -> Const.XML_TAG_UPDATE_GUIDEBOOK
                    UPDATES.MAP -> Const.XML_TAG_UPDATE_MAP
                    UPDATES.CATEGORIES -> Const.XML_TAG_UPDATE_CATEGORIES
                }
                element(tag) {
                    attribute(Const.XML_ATTR_URL, u.url.toExternalForm())
                    attribute(Const.XML_ATTR_SIZE, u.size.toString())
                    if (u.type != UPDATES.CATEGORIES) {
                        attribute(Const.XML_ATTR_UUID, u.uuid.toString())
                        attribute(Const.XML_ATTR_NAME, u.name)
                        attribute(Const.XML_ATTR_VERSION, u.version.toString())
                    }
                }
            }
        }
    }
    writer.flush()
    writer.close()
    outputStreamZip.close()

    println("Zapisano updates: ${file.absoluteFile}\n")
}


fun Pair<Double, Double>.toXmlAttr(): String {
    return arrayOf(this.first, this.second).joinToString(",")
}


fun BufferedImage.resize(scale: Float): BufferedImage {
    val newW = Math.round(width * scale)
    val newH = Math.round(height * scale)
    val tmp = getScaledInstance(newW, newH, Image.SCALE_SMOOTH)
    val dimg = BufferedImage(newW, newH, type)

    val g2d = dimg.createGraphics()
    g2d.drawImage(tmp, 0, 0, null)
    g2d.dispose()

    return dimg
}

fun BufferedImage.scale(maxWidth: Int, maxHeight: Int): Float {
    return Math.min(1.0f * maxWidth / this.width, 1.0f * maxHeight / this.height)
}


fun copyImgFiles(guidebook: GuidebookMetadata) {
    val baseurl = try {
        if (guidebook.baseurl.isNullOrBlank()) {
            null
        } else {
            URL(guidebook.baseurl)
        }
    } catch (e: Exception) {
        null
    }

    val outDir = if (baseurl is URL) {
        if (baseurl.host == Conf.UPDATES_REMOTE_HOST) {
            File(arrayOf(Conf.ROOT, Const.DIR_OUT, baseurl.file).joinToString("/", postfix = "/"))
        } else {
            File(arrayOf(Conf.ROOT, Const.DIR_OUT, baseurl.host, baseurl.file).joinToString("/", postfix = "/"))
        }
    } else {
        File(arrayOf(Conf.ROOT, Const.DIR_OUT, Const.DIR_OUT_DATA, guidebook.uuid).joinToString("/", postfix = "/"))
    }

    Conf.copyFiles.forEach { uri, dest ->
        val destDir = File(outDir, dest.first.toString())
        if (!destDir.exists()) {
            destDir.mkdirs()
        }

        val target = File(destDir, dest.second)
        when (uri.toURL().protocol) {
            "file" -> {
                try {
                    File(uri).copyTo(target, overwrite = false)
                    println(" >> $uri => $target")
                } catch (e: Exception) {
                    println("  E: $uri ${e.message}")
                }
            }
            "http", "https" -> {
                try {
                    val (_, _, result) = Fuel.get(uri.toURL().toExternalForm()).response()
                    val (bytes, _) = result
                    if (bytes != null) {
                        target.writeBytes(bytes)
                        println(" >> $uri => $target")
                    }
                } catch (e: Exception) {
                    println("  E: $uri ${e.message}")
                }
            }
        }

        try {
            if (target.exists()) {
                val image = ImageIO.read(target)
                val scale = image.scale(Conf.maxWidth, Conf.maxHeight)
                if (scale < 1.0) {
                    println("   skanluję: %s (%.1f%%)".format(target.name, scale * 100))
                    val resized = image.resize(scale)
                    ImageIO.write(resized, target.extension, target)
                }
            }
        } catch (e: Exception) {
            println("  E: $target: ${e.message}")
        }
    }
}


fun readContent(root: File, subdir: String? = null, uuid: UUID): String {
    val dir = if (subdir.isNullOrEmpty()) {
        root
    } else {
        File(root, subdir)
    }
    val contentFile = File(dir, Const.FILE_CONTENT_HTM)
    return try {
        if (contentFile.exists() && contentFile.isFile && contentFile.canRead()) {
            val html = Jsoup.parse(contentFile.readText(charset = Charset.forName("UTF-8")))
            html.select("img").forEach { img ->
                val src = URI(img.attr("src").removePrefix("guide://"))

                img.attr("src", File(src.path).name)
                println("  znaleziono img: $src")

                val target = if (src.isAbsolute) src else File(dir, src.path).toURI()
                Conf.copyFiles[target] = Pair(uuid, File(src.path).name)

                try {
                    val image = ImageIO.read(target.toURL())
                    val scale = Math.min(1.0f * Conf.maxWidth / image.width, 1.0f * Conf.maxHeight / image.height)

                    if (scale < 1.0) {
                        img.attr("width", Math.round(image.width * scale).toString())
                        img.attr("height", Math.round(image.height * scale).toString())
                    } else {
                        img.attr("width", image.width.toString())
                        img.attr("height", image.height.toString())
                    }
                } catch (ex: Exception) {
                    println("W: $contentFile - ${ex.message}")
                    img.removeAttr("width")
                    img.removeAttr("height")
                }
            }
            html.body().toString()
                    .replaceFirst("<body>", "")
                    .replace("</body>", "").trim()
        } else {
            String()
        }
    } catch (e: Exception) {
        println("E: $contentFile - ${e.message}")
        String()
    }
}


fun readGuidebookDir(dir: File) {
    Conf.copyFiles.clear()
    val poiList = mutableListOf<Poi>()
    val poiDirMap = mutableMapOf<String, UUID>()
    val pagesList = mutableListOf<Page>()
    val routesList = mutableListOf<Route>()

    println("Odczytuję przewodnik: ${dir.name}")

    val metadataFile = File(dir, Const.FILE_METADATA_JSON)
    if (!metadataFile.isFile || !metadataFile.canRead()) {
        return
    }

    // GUIDE BOOK
    val guidebook: GuidebookMetadata = try {
        Gson().fromJson(metadataFile.readText(), GuidebookMetadata::class.java)
    } catch (e: Exception) {
        e.printStackTrace()
        return
    }
    val xmlOutFileName = dir.nameWithoutExtension + ".xml.gz"
    val geomCenter = Pair(guidebook.center.getOrNull(0) ?: 0.0, guidebook.center.getOrNull(1) ?: 0.0)

    // POI
    val poiDir = File(dir, Const.DIR_POI)
    val poiGeojsonFile = File(poiDir, Const.FILE_POI_GEOJSON)
    try {
        if (poiGeojsonFile.exists() && poiGeojsonFile.canRead() && poiGeojsonFile.isFile) {
            val poiGeojson = Gson().fromJson(poiGeojsonFile.readText(), GeoJsonPoint::class.java)
            poiGeojson.features.forEach { f ->
                val uuid = UUID.fromString(f.properties.uuid)

                val geom = f.geometry.coordinates
                val poi = Poi(
                        uuid = uuid,
                        name = f.properties.name,
                        thumbnail = readThumbnail(poiDir, f.properties.dir),
                        category = f.properties.category,
                        desc = readContent(poiDir, f.properties.dir, uuid),
                        geom = Pair(geom.getOrNull(0) ?: 0.0, geom.getOrNull(1) ?: 0.0)
                )
                poiList.add(poi)
                poiDirMap[f.properties.dir] = uuid
            }
        }
    } catch (e: Exception) {
        println(e.message)
    }

    // PAGES
    val pagesDir = File(dir, Const.DIR_PAGES)
    val pagesJsonFile = File(pagesDir, Const.FILE_PAGES_JSON)
    try {
        if (pagesJsonFile.exists() && pagesJsonFile.canRead() && pagesJsonFile.isFile) {
            val pagesJson = Gson().fromJson<Map<String, Page>>(pagesJsonFile.readText(), typePagesHashMap)
            pagesJson.forEach { d, p ->
                val page = Page(
                        uuid = p.uuid,
                        name = p.name,
                        thumbnail = readThumbnail(pagesDir, d),
                        category = p.category,
                        desc = readContent(pagesDir, d, p.uuid)
                )
                pagesList.add(page)
            }
        }
    } catch (e: Exception) {
        println(e.message)
    }

    // ROUTES
    val routesDir = File(dir, Const.DIR_ROUTES)
    val routesGeojsonFile = File(routesDir, Const.FILE_ROUTES_GEOJSON)
    try {
        if (routesGeojsonFile.exists() && routesGeojsonFile.canRead() && routesGeojsonFile.isFile) {
            val routesGeojson = Gson().fromJson(routesGeojsonFile.readText(), GeoJsonRoute::class.java)
            routesGeojson.features.forEachIndexed { i, f ->
                val g = GeoJsonOnlyUid(
                        type = routesGeojson.type,
                        crs = routesGeojson.crs,
                        features = listOf(
                                GeoJsonFeatureOnlyUid(
                                        type = routesGeojson.features[i].type,
                                        properties = GeoJsonFeatureAttributesOnlyUid(
                                                routesGeojson.features[i].properties.uuid
                                        ),
                                        geometry = routesGeojson.features[i].geometry
                                )
                        )
                )
                val uuid = UUID.fromString(f.properties.uuid)
                val route = Route(
                        uuid = uuid,
                        name = f.properties.name,
                        thumbnail = readThumbnail(routesDir, f.properties.dir),
                        category = f.properties.category,
                        desc = readContent(routesDir, f.properties.dir, uuid),
                        geom = Gson().toJson(g),
                        poi = readRoutesPoi(routesDir, f.properties.dir, poiDirMap)
                )
                routesList.add(route)
            }
        }
    } catch (e: Exception) {
        println(e.message)
    }

    // OUT DIR
    val outDir = File(File(Conf.ROOT, Const.DIR_OUT), Const.DIR_OUT_UPDATES)
    if (!outDir.exists()) {
        outDir.mkdirs()
    }

    // create XML
    val file = File(outDir, xmlOutFileName)
    val outputStream = FileOutputStream(file)
    val outputStreamZip = GZIPOutputStream(outputStream)
    val factory = XMLOutputFactory.newInstance()
    val writer = factory.createXMLStreamWriter(outputStreamZip, "UTF-8")

    val baseurl = try {
        if (guidebook.baseurl.isNullOrBlank()) {
            null
        } else {
            URL(guidebook.baseurl)
        }
    } catch (e: Exception) {
        null
    }
    writer.document {
        element(Const.XML_TAG_GUIDE) {
            attribute(Const.XML_ATTR_NAME, guidebook.name)
            attribute(Const.XML_ATTR_VERSION, guidebook.version.toString())
            attribute(Const.XML_ATTR_UUID, guidebook.uuid.toString())
            attribute(Const.XML_ATTR_CATEGORY, guidebook.category)
            if (baseurl is URL) {
                attribute(Const.XML_ATTR_BASEURL, baseurl.toExternalForm())
            }
            attribute(Const.XML_ATTR_CENTER, geomCenter.toXmlAttr())

            elementCData(Const.XML_TAG_DESCRIPTION, readContent(dir, uuid = guidebook.uuid))
            element(Const.XML_TAG_THUMBNAIL, readThumbnail(dir))

            element(Const.XML_TAG_ATTRACTIONS) {
                poiList.forEach { poi ->
                    element(Const.XML_TAG_POI) {
                        attribute(Const.XML_ATTR_UUID, poi.uuid.toString())
                        attribute(Const.XML_ATTR_POSITION, poi.geom.toXmlAttr())
                        attribute(Const.XML_ATTR_NAME, poi.name)
                        attribute(Const.XML_ATTR_CATEGORY, poi.category)

                        elementCData(Const.XML_TAG_DESCRIPTION, poi.desc)
                        element(Const.XML_TAG_THUMBNAIL, poi.thumbnail)
                    }
                }
            }

            element(Const.XML_TAG_PAGES) {
                pagesList.forEach { page ->
                    element(Const.XML_TAG_PAGE) {
                        attribute(Const.XML_ATTR_UUID, page.uuid.toString())
                        attribute(Const.XML_ATTR_NAME, page.name)
                        attribute(Const.XML_ATTR_CATEGORY, page.category)

                        elementCData(Const.XML_TAG_DESCRIPTION, page.desc)
                        element(Const.XML_TAG_THUMBNAIL, page.thumbnail)
                    }
                }
            }

            element(Const.XML_TAG_ROUTES) {
                routesList.forEach { route ->
                    element(Const.XML_TAG_ROUTE) {
                        attribute(Const.XML_ATTR_UUID, route.uuid.toString())
                        attribute(Const.XML_ATTR_NAME, route.name)
                        attribute(Const.XML_ATTR_CATEGORY, route.category)

                        elementCData(Const.XML_TAG_DESCRIPTION, route.desc)
                        element(Const.XML_TAG_THUMBNAIL, route.thumbnail)
                        element(Const.XML_TAG_GEOMETRY, route.geom)
                        element(Const.XML_TAG_OBJECTS) {
                            route.poi.forEachIndexed { index, uuid ->
                                element(Const.XML_TAG_OBJECT) {
                                    attribute(Const.XML_ATTR_POSITION, (index + 1).toString())
                                    attribute(Const.XML_ATTR_UUID, uuid.toString())
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    writer.flush()
    writer.close()
    outputStreamZip.close()
    val outFileSize = file.length()

    Conf.updates.add(Update(
            size = outFileSize,
            type = UPDATES.GUIDEBOOK,
            name = guidebook.name,
            uuid = guidebook.uuid,
            version = guidebook.version,
            url = URL(
                    Conf.UPDATES_REMOTE_PROTOCOL,
                    Conf.UPDATES_REMOTE_HOST,
                    File("/" + Const.DIR_OUT_UPDATES, xmlOutFileName).canonicalPath)
    )
    )

    println("\n Kopiuję pliki graficzne:")
    copyImgFiles(guidebook)

    println("Zapisano przewodnik: ${file.absoluteFile}\n")
}