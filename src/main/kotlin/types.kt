import com.google.gson.reflect.TypeToken

val typeCategoriesHashMap = object : TypeToken<Map<String, Category>>() {}.type

val typeMapsHashMap = object : TypeToken<Map<String, MapMetadata>>() {}.type

val typePagesHashMap = object : TypeToken<Map<String, Page>>() {}.type

