import java.io.File
import java.util.prefs.Preferences
import javax.swing.JFileChooser
import javax.swing.JOptionPane

class Packager {
    fun packager() {
        val prefs = Preferences.userNodeForPackage(Packager::class.java)
        val lastDirKey = "last_open_dir"

        val fc = JFileChooser()
        fc.fileSelectionMode = JFileChooser.DIRECTORIES_ONLY
        fc.currentDirectory = File(prefs.get(lastDirKey, Conf.ROOT.absolutePath))

        val returnVal = fc.showOpenDialog(null)
        if (returnVal == JFileChooser.APPROVE_OPTION) {
            Conf.ROOT = fc.selectedFile
            prefs.put(lastDirKey, fc.selectedFile.absolutePath)
        } else {
            return
        }

        val dirOut = File(Conf.ROOT, Const.DIR_OUT)
        if (dirOut.exists()) {
            val res = JOptionPane.showConfirmDialog (null, "Katalog docelowy $dirOut istnieje. Usunąć?","Ostrzeżenie", JOptionPane.YES_NO_OPTION)

            if(res == JOptionPane.YES_OPTION) {
                dirOut.deleteRecursively()
            }
        }

        Conf.ROOT.listFiles().filter { it.isDirectory }.forEach { dir ->
            when {
                dir.name.endsWith(".guidebook") -> readGuidebookDir(dir)
                dir.name == Const.DIR_MAPS -> readMaps(dir)
                dir.name == Const.DIR_CATEGORIES -> readCategories(dir)
            }
        }

        writeUpdatesXml()
        println("Zakończono.")
    }
}


fun main(args: Array<String>) {
    Packager().packager()
}
