import java.util.UUID
import java.net.URI
import java.net.URL

enum class UPDATES {
    GUIDEBOOK,
    MAP,
    CATEGORIES
}

data class Category(
        val tags: List<String>,
        val name: Map<String, String>,
        val thumbnail: String
)

data class GeoJsonRoute(
        val type: String,
        val crs: GeoJsonCrs,
        val features: List<GeoJsonFeatureRoute>
)

data class GeoJsonPoint(
        val type: String,
        val crs: GeoJsonCrs,
        val features: List<GeoJsonFeaturePoint>
)


data class GeoJsonOnlyUid(
        val type: String,
        val crs: GeoJsonCrs,
        val features: List<GeoJsonFeatureOnlyUid>
)


data class GeoJsonCrs(
        val type: String,
        val properties: GeoJsonCrsProperties
)

data class GeoJsonCrsProperties(
        val name: String
)

data class GeoJsonFeaturePoint(
        val type: String,
        val properties: GeoJsonFeatureAttributes,
        val geometry: GeoJsonFeatureGeometryPoint
)


data class GeoJsonFeatureOnlyUid(
        val type: String,
        val properties: GeoJsonFeatureAttributesOnlyUid,
        val geometry: GeoJsonFeatureGeometryJson
)

data class GeoJsonFeatureRoute(
        val type: String,
        val properties: GeoJsonFeatureAttributes,
        val geometry: GeoJsonFeatureGeometryJson
)


data class GeoJsonFeatureAttributes(
        val uuid: String,
        val name: String,
        val category: String,
        val dir: String
)

data class GeoJsonFeatureAttributesOnlyUid(
        val uuid: String
)


data class GeoJsonFeatureGeometryPoint(
        val type: String,
        val coordinates: List<Double>
)

data class GeoJsonFeatureGeometryJson(
        val type: String,
        val coordinates: Any
)

data class GuidebookMetadata(
        val name: String,
        val version: Int,
        val uuid: UUID,
        val baseurl: String?,
        val category: String,
        val center: List<Double>
)


data class MapMetadata(
        val uuid: UUID,
        val uri: URI,
        val version: Int
)


data class Page(
        val uuid: UUID,
        val name: String = String(),
        val desc: String = String(),
        val category: String = String(),
        val thumbnail: String = String()
)


data class Poi(
        val uuid: UUID,
        val name: String,
        val desc: String,
        val category: String,
        val thumbnail: String,
        val geom: Pair<Double, Double>
)


data class Route(
        val uuid: UUID,
        val name: String = String(),
        val desc: String = String(),
        val category: String = String(),
        val thumbnail: String = String(),
        val poi: List<UUID> = mutableListOf(),
        val geom: String = String()
)


data class Update(
        val type: UPDATES,
        val uuid: UUID = UUID(0L, 0L),
        val name: String = String(),
        val version: Int = 0,
        val url: URL,
        val size: Long = 0L
)
