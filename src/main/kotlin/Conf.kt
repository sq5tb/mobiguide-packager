import java.io.File
import java.net.URI
import java.util.*


object Conf {
    var ROOT = File(System.getProperty("user.home"))
    const val UPDATES_REMOTE_HOST = "dl.mobiguide.ciastek.eu"
    const val UPDATES_REMOTE_PROTOCOL = "https"
    const val DEFAULT_LANG = "en"

    val copyFiles = mutableMapOf<URI, Pair<UUID, String>>()
    val updates = mutableListOf<Update>()
    const val maxHeight = 500
    const val maxWidth = 300
}
