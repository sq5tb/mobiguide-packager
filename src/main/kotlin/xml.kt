import javax.xml.stream.XMLStreamWriter

fun XMLStreamWriter.document(init: XMLStreamWriter.() -> Unit): XMLStreamWriter {
    this.writeStartDocument()
    this.init()
    this.writeEndDocument()
    return this
}

fun XMLStreamWriter.element(name: String, init: XMLStreamWriter.() -> Unit): XMLStreamWriter {
    this.writeStartElement(name)
    this.init()
    this.writeEndElement()
    return this
}

fun XMLStreamWriter.element(name: String, content: String) {
    element(name) {
        writeCharacters(content)
    }
}

fun XMLStreamWriter.elementCData(name: String, content: String) {
    element(name) {
        if (content.isEmpty()) {
            writeCharacters(content)
        }
        else {
            writeCData(content)
        }
    }
}

fun XMLStreamWriter.attribute(name: String, value: String) = writeAttribute(name, value)
